FROM alpine:latest

MAINTAINER Cornelicorn <dockers@hoffmn.de>
LABEL maintainer="Cornelicorn <dockers@hoffmn.de>"

ARG USER=ncsync
ARG USER_UID=1000
ARG USER_GID=1000

ENV USER=$USER \
    USER_UID=$USER_UID \
    USER_GID=$USER_GID \
    NC_USER=username \
    NC_PASS=password \
    NC_URL="" \
    NC_INTERVAL=300 \
    NC_TRUST_CERT=false \
    NC_SOURCE_DIR="/media/nextcloud/" \
    NC_SILENT=false \
    NC_HIDDEN=false


RUN addgroup -g $USER_GID $USER && adduser -G $USER -D -u $USER_UID $USER

RUN apk update && apk add nextcloud-client && rm -rf /etc/apk/cache

ADD run.sh /usr/bin/run.sh

CMD /usr/bin/run.sh