# [docker.hoffmn.de/nextcloudsync](https://gitlab.com/corndockers/nextcloudsync)
[Nextcloud](https://nextcloud.com/) is a self-hosted file sharing platform.
This container is meant to connect a Nextcloud-Instance and sync all files of a user into a directory on the docker host.

This image is based on the work by [Martin Peters](https://github.com/FreakyBytes)

## Usage
To get started simply run the command below or start the docker-compose listed below.
### docker run
```docker
docker run -d \
    --name nextcloud-client \
    -e NC_USER=username \
    -e NC_PASS=password \
    -e NC_URL="https://nextcloud.example.com" \
    -v nextcloudclient:/media/nextcloud \
    --restart unless-stopped \
    docker.hoffmn.de/nextcloudsync
```
### docker-compose
```docker
version: '3.3'
services:
    nextcloud-client:
        container_name: nextcloud-client
        environment:
            - NC_USER=username
            - NC_PASS=password
            - NC_URL="https://nextcloud.example.com"
        volumes:
            - 'nextcloudclient:/media/nextcloud'
        restart: unless-stopped
        image: docker.hoffmn.de/nextcloudsync
```

## Further configuration
### Data location
The data is synced to `/media/nextcloud` inside the container. This directory can be volume-mounted to the docker host. Using the methods above, the data is synced to the subdirectory `nextcloudclient`. You can change this path to a different folder.
### Environment variables
| Variable | Values | Function |
|---|---|---|
|`NC_USER`| your username | Specifies which users files to sync |
|`NC_PASS`| your password | Authenticates the user to the nextcloud instance. Both the user password and specific app passwords can be used. |
|`NC_URL`| "https://nextcloud.example.com" | URL to the Nextcloud instance |
|`NC_INTERVAL` | integer | Time in seconds to wait between sync attempts |
|`NC_TRUST_CERT` (optional) | false/true | Setting this to true will disable trust verification of the SSL certificate of the Nextcloud instance |
|`NC_HIDDEN` (optional) | false/true | Sync hidden files |
|`NC_SILENT` (optional) | false/true | Hinder verbose output |

## Updating
### docker run
* Pull the latest image: `docker pull docker.hoffmn.de/nextcloudsync`
* Stop and remove the current container `docker stop nextcloud-client && docker rm nextcloud-client`
* Run the command you used to create the container again
### docker-compose
* Pull the latest image: `docker-compose pull`
* Stop the running container: `docker-compose down`
* Restart the container: `docker-compose up -d`
## Troubleshooting
* Show logs: `docker logs -f nextcloud-client`
* Shell access: `docker exec -it nextcloud-client /bin/ash`

## Build yourself
```
git clone https://github.com/docker.hoffmn.de/nextcloudsync-docker.git
cd nextcloud-client-docker
docker build --no-cache --pull -t docker.hoffmn.de/nextcloudsync:latest .
```